import React, {useState, useEffect} from 'react';
import Link from 'next/link';
import axios from 'axios';
import CardLoading from '../component/CardLoading';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {FacebookShareButton, FacebookIcon, WhatsappShareButton, WhatsappIcon, TwitterShareButton, TwitterIcon} from 'react-share';


function Card() {
    const [datadata, setData] = useState([]);
    const [username, setUsername] = useState("");
    const [Loading, setLoading] = useState(true);
    const [isShared, setShared] = useState("");
    const [isClicked, setClick] = useState(false);
    useEffect(() => {
        setUsername(localStorage.getItem("name"));
        axios.get(
        `https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed`
        )
        .then(response => {
            setData(response.data);
            localStorage.setItem("datalength", response.data.length);
            setLoading(false);
        })
    }, [])

    const shared = (sharedid) => {
        setShared(sharedid);
        setClick(!isClicked);
    }
    const likeit = async (value, oneava, onecover, onelikers, onelikes, onestory, onetitle, onewriter ) => {
        await axios.get(
            `https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/${value - 1}`
            )
            .then(responseone => {
        })


        if (!onelikers.includes(username)) {
        await fetch(`https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/${value - 1}`, {
            method: "PUT",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ava: oneava,
                cover: onecover,
                id: value,
                likers: onelikers + "," + username,
                likes: parseInt(onelikes) + 1,
                story: onestory,
                title: onetitle,
                writer: onewriter,
            }),
            })
            .then((r) => r.json())
            .catch();

            await axios.get(
                `https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed`
                )
                .then(response => {
                    setData(response.data);
            })
        }else{
            await fetch(`https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/${value - 1}`, {
            method: "PUT",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ava: oneava,
                cover: onecover,
                id: value,
                likers: onelikers.replace(","+username, ""),
                likes: parseInt(onelikes) + 1,
                story: onestory,
                title: onetitle,
                writer: onewriter,
            }),
            })
            .then((r) => r.json())
            .catch();

            await axios.get(
                `https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed`
                )
                .then(response => {
                    setData(response.data);
                    localStorage.setItem("datalength", datadata.length);
            })
        }
    }
    return (
        <div className="flex flex-wrap">
            {Loading ? <CardLoading/> : (
            <div className="flex flex-wrap justify-between">
            {datadata.map((datum) => { return( 
            <div key={datum.id} className="bg-white shadow-md mx-5 my-5 rounded-lg lg:w-1/5 md:w-1/4 sm:w-1/3 w-full">
                <div className="px-4 py-4 text-pink-500">
                    <h1 className="font-semibold text-lg lg:text-xl mb-2">{datum.title}</h1>
                    <img className="w-full h-52 object-cover" src={datum.cover === "" ? "https://drive.google.com/uc?export=view&id=1cxd6i4Tkj37e4mIl0YYze5K68MFEqx8s" : datum.cover} alt=""/>
                    <p className="">{datum.story.substring(0, 100)}...</p>
                    <div className="mt-3 flex items-center">
                        <img className="h-8 w-8 rounded-full object-cover" src={datum.ava === "" ? "https://drive.google.com/uc?export=view&id=1Sy6sMRzC4cAjIREVnaKEfXJVApJAv_nM" : datum.ava} alt=""/>
                        <h1 className="ml-2 bg-pink-500 rounded-full text-white px-2">{datum.writer}</h1>
                    </div>
                    <div className="flex mt-3 items-center justify justify-between">
                        <div className="flex">
                            <svg name="love" onClick={() =>{ toast.success("Clicked! wait a few seconds."); likeit(datum.id, datum.ava, datum.cover, datum.likers, datum.likes, datum.story, datum.title, datum.writer);}} className={`cursor-pointer h-6 w-6 focus:outline-none love-btn hover:text-pink-500 ${datum.likers.includes(username) ? "text-pink-500" : "text-gray-400"}`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                            </svg>
                            <svg onClick={() => shared(datum.id)} className={`cursor-pointer h-6 w-6 ml-2 text-gray-400`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M15 8a3 3 0 10-2.977-2.63l-4.94 2.47a3 3 0 100 4.319l4.94 2.47a3 3 0 10.895-1.789l-4.94-2.47a3.027 3.027 0 000-.74l4.94-2.47C13.456 7.68 14.19 8 15 8z" />
                            </svg>
                            <div className={`${isClicked === true && datum.id === isShared ? "block" : "hidden"}`}>
                            <FacebookShareButton className="outline-none focus:outline-none" url={`https://tellastory.vercel.app/detail?id=${datum.id}`} quote={"Hi everyone, I've just read this interesting story. Check this out!"} hashtag="#TellaStory">
                                <FacebookIcon className="outline-none focus:outline-none h-6 w-6 ml-2"  round={true}>
                                </FacebookIcon>
                            </FacebookShareButton>
                            <WhatsappShareButton className="outline-none focus:outline-none" url={`https://tellastory.vercel.app/detail?id=${datum.id}`} quote={"Hi everyone, I've just read this interesting story. Check this out!"} hashtag="#TellaStory">
                                <WhatsappIcon className="outline-none focus:outline-none h-6 w-6 ml-2"  round={true}>
                                </WhatsappIcon>
                            </WhatsappShareButton>
                            <TwitterShareButton className="outline-none focus:outline-none" url={`https://tellastory.vercel.app/detail?id=${datum.id}`} quote={"Hi everyone, I've just read this interesting story. Check this out!"} hashtag="#TellaStory">
                                <TwitterIcon className="outline-none focus:outline-none h-6 w-6 ml-2"  round={true}>
                                </TwitterIcon>
                            </TwitterShareButton>
                            </div>
                        </div>
                        <div className={`${isClicked && datum.id === isShared ? "hidden" : "flex"} items-center`}>
                            <Link href={{
                                pathname: "/detail",
                                query: { id: datum.id },
                            }}>
                                <h1 className="cursor-pointer"><em>Read More <span className="text-2xl">&#8594;</span></em></h1>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
               )
            })
          }
          </div>
          )}
        </div>
    )
}

export default Card;
