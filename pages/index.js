import Head from 'next/head';
import Link from 'next/link';
import { ToastContainer } from 'react-toastify';
import Card from '../component/Card';
import Navbar from '../component/Navbar';


export default function Home() {
  return (
    <div className="">
      <Head>
        <title>Tella Story</title>
      </Head>
      <Navbar/>
      <ToastContainer/>
      <Link href="createstory">
      <div className="mt-6">
          <div className="animate-bounce cursor-pointer text-center rounded-xl bg-pink-500 max-w-xs mx-auto py-2">
            <button className="text-white font-bold focus:outline-none">Tell a new story <span className="font-extrabold">+</span></button>
          </div>
      </div>
      </Link>
      <Card/>
    </div>
  )
}
