import {useEffect} from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function Createstory() {
    const router = useRouter();
    useEffect(() => {
        if (localStorage.getItem("name") === null || localStorage.getItem("name") === null) {
            router.push("login");
        }
    }, [])

    const handleCreateStory = async (e) => {
        e.preventDefault();
        const data = {
          writer: localStorage.getItem("name"),
          title: e.target.title.value,
          story: e.target.story.value,
          likes: 0,
          likers: "belumadalikenya",
          id: parseInt(localStorage.getItem("datalength")) + 1
        };
        await fetch("https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed", {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
            })
            .then((r) => r.json())
            .then((data) => {
                // The response comes here
                // router.push("/");
                toast.success("Your story is published successfully!", {
                });
                setTimeout(function() {
                    router.push("/");
                }, 3000)
            })
            .catch((error) => {
                // Errors are reported there
            });
    };
    return (
        <div>
            <ToastContainer/>
            <div className=" my-5 bg-white text-center max-w-xs lg:max-w-md mx-auto shadow-lg">
                <h1 className="text-pink-500 font-semibold text-xl py-3">Create a new story</h1>
            </div>
            <div className=" my-5 bg-white text-center max-w-xs lg:max-w-md mx-auto shadow-lg">
                <form onSubmit={(e) => handleCreateStory(e)}>
                    <h1 className="text-pink-500 font-semibold text-md py-3">- Title -</h1>
                    <input placeholder="what's your story about?" className="w-3/4 p-2 mb-6 text-pink-400 border-b-2 border-pink-400 focus:outline-none focus:bg-pink-50" type="text" name="title"/>
                    <h1 className="text-pink-500 font-semibold text-md py-3">- Story -</h1>
                    <textarea placeholder="tell us your story" className="w-3/4 p-2 mb-6 text-pink-400 border-b-2 border-pink-400 focus:outline-none focus:bg-pink-50" name="story" id="" cols="30" rows="5"></textarea>
                    <div className="py-5">
                        <button className="bg-pink-500 rounded-lg px-3 py-2 w-3/4 text-white font-semibold focus:outline-none">Publish Story</button>
                    </div>
                    <div className="pb-5">
                        <Link href="/">
                            <a className="text-pink-500">Cancel</a>
                        </Link>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Createstory;
