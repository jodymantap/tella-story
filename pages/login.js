import {useEffect} from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from "next/router";

function Login() {
    const router = useRouter();
    const handleLogin = async (e) => {
        localStorage.setItem("name", e.target.name.value);
        router.push("createstory");
    };

    useEffect(() => {
        if (localStorage.getItem("name") !== null) {
            router.push("createstory");
        }
    }, [])
    return (
        <div>
            <Head>
                <title>Tella Story - Login</title>
            </Head>
            <div className="mx-auto my-10 text-center">
                <Link href="/">
                    <div className="width-min max-w-xs mx-auto flex items-center cursor-pointer">
                        <svg className="h-8 w-8 text-pink-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm.707-10.293a1 1 0 00-1.414-1.414l-3 3a1 1 0 000 1.414l3 3a1 1 0 001.414-1.414L9.414 11H13a1 1 0 100-2H9.414l1.293-1.293z" clip-rule="evenodd" />
                        </svg>
                        <h1 className="text-pink-500 text-xl">Back</h1>
                    </div>
                </Link>
                <div className="flex h-full bg-silk">
                    <div className="max-w-xs w-full m-auto bg-white rounded p-5 shadow-lg">
                        <header>
                            <div> 
                                <h1 className="mb-5 text-pink-600 text-4xl font-bold">Login</h1>
                            </div>
                            <div>
                                <h1 className="mb-5 text-white text-md rounded-full font-bold bg-pink-500">Please enter your name first</h1>
                                <h1 className="mb-5 text-pink-500 text-xs rounded-full font-bol">*We don't have time to collect your authentication data, you don't even need any password.</h1>
                            </div>
                        </header>
                        <form onSubmit={(e) => handleLogin(e)}>
                            <div>
                                <label class="block mb-2 text-pink-500" for="username">Name</label>
                                <input class="w-full p-2 mb-6 text-pink-400 border-b-2 border-pink-400 focus:outline-none focus:bg-pink-50" type="text" name="name"/>
                            </div>
                            <div>          
                                <button class="w-full bg-pink-600 cursor-pointer text-white font-bold py-2 px-4 mb-6 rounded">Login</button>
                            </div>      
                        </form>
                        <footer>
                            <img className="w-20 mx-auto my-5" src="https://drive.google.com/uc?export=view&id=1_Maz2EbNc59nNQrd7rTHKZNmRTQaLuLZ" />
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;
