import {useState, useEffect} from 'react';
import axios from 'axios';
import Link from 'next/link';
import CardLoading from '../component/CardLoading';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {FacebookShareButton, FacebookIcon, WhatsappShareButton, WhatsappIcon, TwitterShareButton, TwitterIcon} from 'react-share';

function detail(paramsid) {
    const [oneData, setOneData] = useState([]);
    const [username, setUsername] = useState("");
    const [likers, setLikers] = useState("");
    const [isClicked, setClick] = useState(false);
    useEffect( () => {
        setUsername(localStorage.getItem("name"));
        axios.get(
        `https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/id/${paramsid.paramsid}`
        )
        .then(response => {
            setOneData(response.data[0]);
            setLikers(response.data[0].likers);
        })
    }, [])

    const likeit = async () => {

        if (!likers.includes(username)) {
        await fetch(`https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/${(paramsid.paramsid) - 1}`, {
            method: "PUT",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ava: oneData.ava,
                cover: oneData.cover,
                id: oneData.id,
                likers: oneData.likers + "," + username,
                likes: parseInt(oneData.likes) + 1,
                story: oneData.story,
                title: oneData.title,
                writer: oneData.writer,
            }),
            })
            .then((r) => r.json())
            .catch();
            await axios.get(
                `https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/id/${paramsid.paramsid}`
                )
                .then(response => {
                    // setOneData(response.data[0]);
                    setLikers(response.data[0].likers);
            })
        }else{
            await fetch(`https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/${(paramsid.paramsid) - 1}`, {
            method: "PUT",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ava: oneData.ava,
                cover: oneData.cover,
                id: oneData.id,
                likers: oneData.likers.replace(","+username, ""),
                likes: parseInt(oneData.likes) - 1,
                story: oneData.story,
                title: oneData.title,
                writer: oneData.writer,
            }),
            })
            .then((r) => r.json())
            .catch();

            await axios.get(
                `https://sheet.best/api/sheets/2f46a4fe-fa9e-4f51-912c-a37c375e23ed/id/${paramsid.paramsid}`
                )
                .then(response => {
                    // setOneData(response.data[0]);
                    setLikers(response.data[0].likers);
            })
        }
    }
    return (
        <div>
        <ToastContainer/>
        <div className="flex justify-center">
            <div className="bg-white shadow-md mx-5 my-5 rounded-lg lg:w-1/2 md:w-1/2 sm:w-1/2 w-full">
                <Link href="/">
                    <div className="flex items-center cursor-pointer">
                        <svg className="h-8 w-8 text-pink-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm.707-10.293a1 1 0 00-1.414-1.414l-3 3a1 1 0 000 1.414l3 3a1 1 0 001.414-1.414L9.414 11H13a1 1 0 100-2H9.414l1.293-1.293z" clip-rule="evenodd" />
                        </svg>
                        <h1 className="text-pink-500 text-xl">Back</h1>
                    </div>
                </Link>
                <div className="px-4 py-4 text-pink-500">
                    <h1 className="font-semibold text-2xl lg:text-3xl mb-2">{oneData.title}</h1>
                    <img className="w-full h-100 object-cover" src={oneData.cover === "" ? "https://drive.google.com/uc?export=view&id=1cxd6i4Tkj37e4mIl0YYze5K68MFEqx8s" : oneData.cover} alt=""/>
                    <div className="mt-3 flex items-center">
                        <img className="h-8 w-8 rounded-full object-cover" src={oneData.ava === "" ? "https://drive.google.com/uc?export=view&id=1Sy6sMRzC4cAjIREVnaKEfXJVApJAv_nM" : oneData.ava} alt=""/>
                        <h1 className="ml-2 bg-pink-500 rounded-full text-white px-2">{oneData.writer}</h1>
                    </div>
                    <p className="mt-3">{oneData.story}</p>
                    <div className="flex mt-3 items-center justify justify-between">
                        <div className="flex">
                            <svg name="love" onClick={() =>{ toast.success("Clicked! wait a few seconds."); likeit();}} className={`cursor-pointer h-6 w-6 focus:outline-none focus:text-pink-500 ${likers.includes(username) ? "text-pink-500" : "text-gray-400"}`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                            </svg>
                            <svg onClick={() => setClick(!isClicked)} className="cursor-pointer h-6 w-6 ml-2 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M15 8a3 3 0 10-2.977-2.63l-4.94 2.47a3 3 0 100 4.319l4.94 2.47a3 3 0 10.895-1.789l-4.94-2.47a3.027 3.027 0 000-.74l4.94-2.47C13.456 7.68 14.19 8 15 8z" />
                            </svg>
                            <div className={`${isClicked === true ? "block" : "hidden"}`}>
                            <FacebookShareButton className="outline-none focus:outline-none" url={`https://tellastory.vercel.app/detail?id=${oneData.id}`} quote={"Hi everyone, I've just read this interesting story. Check this out!"} hashtag="#TellaStory">
                                <FacebookIcon className="outline-none focus:outline-none h-6 w-6 ml-2"  round={true}>
                                </FacebookIcon>
                            </FacebookShareButton>
                            <WhatsappShareButton className="outline-none focus:outline-none" url={`https://tellastory.vercel.app/detail?id=${oneData.id}`} quote={"Hi everyone, I've just read this interesting story. Check this out!"} hashtag="#TellaStory">
                                <WhatsappIcon className="outline-none focus:outline-none h-6 w-6 ml-2"  round={true}>
                                </WhatsappIcon>
                            </WhatsappShareButton>
                            <TwitterShareButton className="outline-none focus:outline-none" url={`https://tellastory.vercel.app/detail?id=${oneData.id}`} quote={"Hi everyone, I've just read this interesting story. Check this out!"} hashtag="#TellaStory">
                                <TwitterIcon className="outline-none focus:outline-none h-6 w-6 ml-2"  round={true}>
                                </TwitterIcon>
                            </TwitterShareButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}

 // this function only runs on the server by Next.js
 export async function getServerSideProps(params) {
     const paramsid = params.query.id;
    return {
      props: {paramsid}, // will be passed to the page component as props
    }
  }

export default detail;
