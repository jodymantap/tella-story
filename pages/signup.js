import Head from 'next/head';
import Link from 'next/link';


function Signup() {
    return (
        <div>
            <div className="mx-auto my-10 text-center">
                <Head>
                    <title>Tella Story - Register</title>
                </Head>
                <Link href="/">
                    <div className="width-min max-w-xs mx-auto flex items-center cursor-pointer">
                        <svg className="h-8 w-8 text-pink-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm.707-10.293a1 1 0 00-1.414-1.414l-3 3a1 1 0 000 1.414l3 3a1 1 0 001.414-1.414L9.414 11H13a1 1 0 100-2H9.414l1.293-1.293z" clip-rule="evenodd" />
                        </svg>
                        <h1 className="text-pink-500 text-xl">Back</h1>
                    </div>
                </Link>
                <div className="flex h-full">
                    <div className="max-w-xs w-full m-auto bg-white rounded p-5 shadow-lg">
                        <header>
                            <div>
                                <h1 className="mb-5 text-pink-600 text-4xl font-bold">Register</h1>
                            </div>
                        </header>
                        <form>
                            <div>
                                <label className="block mb-2 text-pink-500" for="username">Full Name</label>
                                <input className="w-full p-2 mb-6 text-pink-400 border-b-2 border-pink-400 focus:outline-none focus:bg-pink-50" type="text" name="fullName"/>
                            </div>
                            <div>
                                <label className="block mb-2 text-pink-500" for="username">Username</label>
                                <input className="w-full p-2 mb-6 text-pink-400 border-b-2 border-pink-400 focus:outline-none focus:bg-pink-50" type="text" name="userName"/>
                            </div>
                            <div>
                                <label className="block mb-2 text-pink-500" for="password">Password</label>
                                <input className="w-full p-2 mb-6 text-pink-400 border-b-2 border-pink-400 focus:outline-none focus:bg-pink-50" type="password" name="password"/>
                            </div>
                            <div>          
                                <button className="w-full bg-pink-600 cursor-pointer text-white font-bold py-2 px-4 mb-6 rounded" type="submit">Register</button>
                            </div>
                            <Link href="login">
                            <div>
                                <a className="text-pink-500 cursor-pointer">Already have account? <span className="underline">sign in</span></a>
                            </div>
                            </Link>       
                        </form>
                        <footer>
                            <img className="w-20 mx-auto my-5" src="https://drive.google.com/uc?export=view&id=1_Maz2EbNc59nNQrd7rTHKZNmRTQaLuLZ" />
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Signup;
