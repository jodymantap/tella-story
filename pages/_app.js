import 'tailwindcss/tailwind.css';
import '../component/style/style.css';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
